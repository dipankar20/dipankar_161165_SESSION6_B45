<h2>basename</h2>
<hr/>
<?php
$path = "web/test.php";
echo basename($path);
echo "<br/>";
echo basename($path,".php");
echo "<br/>";
?>
<h2>dirname</h2>
<hr/>
<?php
echo dirname($path);
echo "<br/>";
?>
<h2>file_get_contents</h2>
<hr/>
<?php
echo file_get_contents("test.txt");
?>
<h2>file_put_contents</h2>
<hr/>
<?php
echo file_put_contents("test.txt","I am learning PHP");
?>
<h2>filesize</h2>
<hr/>
<?php
echo filesize("test.txt");
?>
<h2>filetype</h2>
<hr/>
<?php
echo filetype("test.txt");
?>
<h2>fread</h2>
<hr/>
<?php
$file=fopen("test.txt","r");
echo fread($file,12);
?>
<h2>pathinfo</h2>
<hr/>
<?php
print_r(pathinfo($path));
?>
<h2>unlink</h2>
<hr/>
<?php
$file = fopen("test.html","a");
fwrite($file,'<h3>How are you?</h3>');
fclose($file);
unlink('test.html');
?>